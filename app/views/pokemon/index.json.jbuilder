json.array! @pokemons do |pokemon|
  json.partial! 'pokemon', pokemon: pokemon, show_toys: false
end

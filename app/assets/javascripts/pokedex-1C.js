Pokedex.RootView.prototype.createPokemon = function (attrs, callback) {
  this.pokes.create(attrs, {
    wait: true,
    success: function(pokemon) {
      callback(pokemon);
      this.addPokemonToList(pokemon);
    }.bind(this)
  });
};

Pokedex.RootView.prototype.submitPokemonForm = function (event) {
  event.preventDefault();
  var $target = $(event.currentTarget);
  var newPokemonAttrs = $target.serializeJSON().pokemon;
  this.createPokemon(newPokemonAttrs, this.renderPokemonDetail.bind(this));
};

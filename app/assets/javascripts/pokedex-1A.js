Pokedex.RootView.prototype.addPokemonToList = function (pokemon) {
  var $li = $("<li></li>");
  $li.addClass("poke-list-item");
  $li.data("id", pokemon.id);
  $li.append("Name: " + pokemon.escape("name")).append("<br>Type: " + pokemon.escape("poke_type"));
  this.$pokeList.append($li);
};

Pokedex.RootView.prototype.refreshPokemon = function () {
  this.pokes.fetch({
    success: function(){
      this.pokes.forEach (function (pokemon) {
        this.addPokemonToList(pokemon);
      }.bind(this))
    }.bind(this)
  });
};

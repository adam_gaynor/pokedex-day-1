Pokedex.RootView.prototype.addToyToList = function (toy) {
  var $toy = $("<li>");
  $toy.addClass("toy-list-item");
  $toy.append("<br>Name: " + toy.escape("name"));
  $toy.append("<br>Happiness: " + toy.escape("happiness"));
  $toy.append("<br>Price: " + toy.escape("price"));
  $toy.data("id", toy.id);
  $toy.data("pokemon-id", toy.get("pokemon_id"));
  return $toy;
};

Pokedex.RootView.prototype.renderToyDetail = function (toy) {
  var $detail = $("<section>");
  $detail.addClass("detail");
  $detail.append("<img src=\"" + toy.escape("image_url") + "\">");
  this.$toyDetail.html($detail);
};

Pokedex.RootView.prototype.selectToyFromList = function (event) {
  var $toy = $(event.currentTarget);
  var id = $toy.data("id");
  var pokemon_id = $toy.data("pokemon-id");
  var pokemon = this.pokes.get(pokemon_id);
  this.renderToyDetail(pokemon.toys().get(id));
};

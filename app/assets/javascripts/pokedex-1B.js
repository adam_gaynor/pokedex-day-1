Pokedex.RootView.prototype.renderPokemonDetail = function (pokemon) {
  var $detail = $("<section>");
  $detail.addClass("detail");
  $detail.append("<img src=\"" + pokemon.escape("image_url") + "\">");
  $detail.append(pokemon.escape("name"));
  $detail.append("<br><br>TYPE: " + pokemon.escape("poke_type"));
  $detail.append("<br><br>ATK: " + pokemon.escape("attack"));
  $detail.append("<br>DEF: " + pokemon.escape("defense"));

  var $moveSet = pokemon.escape("moves");
  $detail.append("<br><br>MOVES: ");
  $moveSet = $moveSet.split(",");
  $moveSet.forEach(function(move){
    $detail.append("<br>" + move);
  });


  var $toysList = $("<ul>");
  $toysList.addClass("toys");
  $detail.append($toysList);
  var $toys = pokemon.toys();
  $toys.forEach(function(toy){
    $toysList.append(this.addToyToList(toy));
  }.bind(this));
  this.$pokeDetail.html($detail);
};

Pokedex.RootView.prototype.selectPokemonFromList = function (event) {
  var $li = $(event.currentTarget);
  var id = $li.data("id");
  this.pokes.get(id).fetch({
    success: this.renderPokemonDetail.bind(this)
  })
};
